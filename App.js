import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import BottomTabNavigation from './src/navigation/BottomTabNavigation'
import * as Font from 'expo-font';

const App = () => {

  const [loading, setLoading] = useState(true)

  useEffect( () => {
    const fetchData = async () => {
        const response =  await Font.loadAsync({
        'Parisienne-Regular' : require('./assets/fonts/Parisienne-Regular.ttf'),
        'Myriad-Pro-Bold-Condensed': require('./assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
        'Myriad-Pro-Condensed' : require('./assets/fonts/Myriad-Pro-Condensed.ttf')

    });
    setLoading(false);}
    fetchData();
})
  return (
  <View style={{backgroundColor:'red', width:'100%', height:'100%'}}>
    {!loading
    ?<BottomTabNavigation />
    :null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
