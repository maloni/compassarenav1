import React from 'react';
import { Text, View, Image } from 'react-native';

export const SoupIcon = () => {
    return(
        <View style={{alignItems:'center',justifyContent:'center', paddingBottom:10}}>
            <Image style={{width:'50%', height:'50%'}} source={require('../src/img/pot.png')}/>

        </View>
    )
};
