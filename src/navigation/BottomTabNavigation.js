import { createAppContainer } from 'react-navigation'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import HomeNavigation from './HomeNavigation';
import Menu from '../screen/MenuScreen';
import Events from '../screen/EventsScreen';
import MenuScreen from '../screen/MenuScreen';
import EventsScreen from '../screen/EventsScreen';
import React from 'react';
import { View } from 'react-native';

const BottomTabNavigation = createMaterialBottomTabNavigator(
    {
        Home: { 
            screen: HomeNavigation,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <Ionicons
                        name='md-home'
                        color='#000000'
                        size={30}
                        />
                    </View>
                )
            }
        },
        Menu : {
            screen: MenuScreen,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <MaterialIcons
                        name='restaurant-menu'
                        color='#000000'
                        size={30}
                        />
                    </View>
                )
            }
        
        },
        Events: {
            screen: EventsScreen,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <Entypo
                        name='calendar'
                        color='black'
                        size={30}
                        />
                    </View>
                )
            }
        },
        // Drawer: {
        //     screen: DrawerScreen,
        //     navigationOptions:{
        //         tabBarIcon: () => (
        //             <View style={{width:'150%', height:'150%'}}>
        //                 <TabBarIconsMore
        //                 />
        //             </View>
        //         ),
        //     tabBarOnPress: ({ navigation }) => navigation.openDrawer()
        //     }},
    },
    {
        initialRouteName: 'Home',
        activeColor:'black',
        inactiveColor:'black',
        barStyle: { backgroundColor: '#efceb8' },
        labeled: false,
    }
)


export default createAppContainer(BottomTabNavigation);