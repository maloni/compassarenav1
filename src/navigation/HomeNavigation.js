import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../screen/HomeScreen';
import EventsScreen from '../screen/EventsScreen';
import MenuScreen from '../screen/MenuScreen';
import MealScreen from '../screen/MealScreen';
import Meal from '../screen/Meal';
import ReserveScreen from '../screen/ReserveScreen';
import {Entypo } from '@expo/vector-icons';
import React from 'react'

const HomeNavigation = createStackNavigator(
    {
        Home: HomeScreen,
        Events: EventsScreen,
        Menu : MenuScreen,
        Meals : MealScreen,
        Meal : Meal,
        Reserve : ReserveScreen
},{
    defaultNavigationOptions: {
        headerTitleStyle:{
            fontFamily:'Parisienne-Regular',
            fontSize:28
        },
        headerStyle: {
            backgroundColor: '#efceb8',
          },
        headerTintColor: '#000000',
    }
}
);

export default createAppContainer(HomeNavigation)

