import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import * as Font from 'expo-font';
import FontAwesome  from '@expo/vector-icons/FontAwesome';
import Entypo from '@expo/vector-icons/Entypo';

const EventsScreen = () => {

    const [loading, setLoading] = useState(true)

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf'),
            'Myriad-Pro-Bold-Condensed': require('../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('../../assets/fonts/Myriad-Pro-Condensed.ttf')
        });
        setLoading(false);}
        fetchData();
    })

    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/events.jpg')}
            style={{height:'100%'}}
            ><View style={{width:'100%', height:'100%', marginTop:'20%'}}>
                <View style={{width:'80%', borderWidth:1, height:'15%', borderColor:'#efceb8', alignSelf:'center', flexDirection:'row'}}>
                    <View style={{width:'30%', height:'100%', backgroundColor:'#efceb8', justifyContent:'center'}}>
                        <Text style={{fontSize:42, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center',}}>08</Text>
                        <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MAR</Text>
                    </View>
                    <View style={{width:'70%', height:'100%'}}>
                        <Text style={{fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8', marginTop:5}}>Opening Day</Text>
                        <View style={{width:'100%', height:'60%'}}>
                            <View style={{width:'100%', height:'50%', flexDirection:'row', alignItems:'center'}}>
                        <FontAwesome 
                        name='clock-o'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:15}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>09 PM</Text>
                        </View>
                        <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                        <Entypo 
                        name='location-pin'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:13}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>Compass Arena Premier Lounge</Text>
                        </View>
                        </View>
                    </View>
                </View>
                <View style={{width:'80%', borderWidth:1, height:'15%', borderColor:'#efceb8', alignSelf:'center', flexDirection:'row', marginTop:20}}>
                    <View style={{width:'30%', height:'100%', backgroundColor:'#efceb8', justifyContent:'center'}}>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center',}}>14</Text>
                        <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MAR</Text>
                    </View>
                    <View style={{width:'70%', height:'100%'}}>
                        <Text style={{fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8', marginTop:5}}>Italian Cuisine</Text>
                        <View style={{width:'100%', height:'60%'}}>
                            <View style={{width:'100%', height:'50%', flexDirection:'row', alignItems:'center'}}>
                        <FontAwesome 
                        name='clock-o'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:15}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>12 PM</Text>
                        </View>
                        <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                        <Entypo 
                        name='location-pin'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:13}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>Compass Arena Premier Lounge</Text>
                        </View>
                        </View>
                    </View>
                </View>
                <View style={{width:'80%', borderWidth:1, height:'15%', borderColor:'#efceb8', alignSelf:'center', flexDirection:'row', marginTop:20}}>
                    <View style={{width:'30%', height:'100%', backgroundColor:'#efceb8', justifyContent:'center'}}>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center',}}>17</Text>
                        <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MAR</Text>
                    </View>
                    <View style={{width:'70%', height:'100%'}}>
                        <Text style={{fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8', marginTop:5}}>Family Day</Text>
                        <View style={{width:'100%', height:'60%'}}>
                            <View style={{width:'100%', height:'50%', flexDirection:'row', alignItems:'center'}}>
                        <FontAwesome 
                        name='clock-o'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:15}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>05 PM</Text>
                        </View>
                        <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                        <Entypo 
                        name='location-pin'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:13}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>Compass Arena Premier Lounge</Text>
                        </View>
                        </View>
                    </View>
                </View>
                <View style={{width:'80%', borderWidth:1, height:'15%', borderColor:'#efceb8', alignSelf:'center', flexDirection:'row', marginTop:20}}>
                    <View style={{width:'30%', height:'100%', backgroundColor:'#efceb8', justifyContent:'center'}}>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>22</Text>
                        <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MAR</Text>
                    </View>
                    <View style={{width:'70%', height:'100%'}}>
                        <Text style={{fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8', marginTop:5}}>Italian Cuisine</Text>
                        <View style={{width:'100%', height:'60%'}}>
                            <View style={{width:'100%', height:'50%', flexDirection:'row', alignItems:'center'}}>
                        <FontAwesome 
                        name='clock-o'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:15}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>09 PM</Text>
                        </View>
                        <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                        <Entypo 
                        name='location-pin'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:13}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>Compass Arena Premier Lounge</Text>
                        </View>
                        </View>
                    </View>
                </View>
                <View style={{width:'80%', borderWidth:1, height:'15%', borderColor:'#efceb8', alignSelf:'center', flexDirection:'row', marginTop:20}}>
                    <View style={{width:'30%', height:'100%', backgroundColor:'#efceb8', justifyContent:'center'}}>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>27</Text>
                        <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MAR</Text>
                    </View>
                    <View style={{width:'70%', height:'100%'}}>
                        <Text style={{fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8', marginTop:5}}>Taste Of China</Text>
                        <View style={{width:'100%', height:'60%'}}>
                            <View style={{width:'100%', height:'50%', flexDirection:'row', alignItems:'center'}}>
                        <FontAwesome 
                        name='clock-o'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:15}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>09 PM</Text>
                        </View>
                        <View style={{width:'100%', flexDirection:'row', alignItems:'center'}}>
                        <Entypo 
                        name='location-pin'
                        size={14}
                        color='#efceb8'
                        style={{paddingLeft:13}}
                        />
                        <Text style={{fontSize:12, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', color:'#efceb8', paddingLeft:10}}>Compass Arena Premier Lounge</Text>
                        </View>
                        </View>
                    </View>
                </View>

                </View>
            </ImageBackground>
            :null}
        </View>
    )
}

const styles = StyleSheet.create({

});

export default EventsScreen;