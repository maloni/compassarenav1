import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import * as Font from 'expo-font';

const HomeScreen = ({ navigation }) => {

    const [loading, setLoading] = useState(true)

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf'),
            'Myriad-Pro-Bold-Condensed': require('../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('../../assets/fonts/Myriad-Pro-Condensed.ttf')

        });
        setLoading(false);}
        fetchData();
    })

    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/restaurant.jpg')}
            style={{height:'100%'}}
            >
                <View style={{width:'100%', height:'100%', backgroundColor:'rgba(0,0,0,0.7)'}}>
                <Text>
                    Compass ARENA
                </Text>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Events')}
                style={{borderWidth:2, borderColor:'#efceb8', width:'75%', alignItems:'center', marginTop:'70%', alignSelf:'center'}}>
                    <Text style={{fontSize:30, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}> Events </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Menu')}
                style={{borderWidth:2, borderColor:'#efceb8', width:'75%', alignItems:'center', marginTop:'10%', alignSelf:'center'}}>
                    <Text style={{fontSize:30, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular'}}> Menu </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Reserve')}
                style={{borderWidth:2, borderColor:'#efceb8', width:'75%', alignItems:'center', marginTop:'10%', alignSelf:'center'}}>
                    <Text style={{fontSize:30, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular'}}> Reserve Table </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{borderWidth:2, borderColor:'#efceb8', width:'75%', alignItems:'center', marginTop:'10%', alignSelf:'center'}}>
                    <Text style={{fontSize:30, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular'}}> Gallery </Text>
                </TouchableOpacity>
                </View>
            </ImageBackground>
            :null}
        </View>
    )
}

const styles = StyleSheet.create({

});

export default HomeScreen;