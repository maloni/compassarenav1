import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity, Image} from 'react-native';
import * as Font from 'expo-font';
import { FontAwesome } from '@expo/vector-icons';

const Meal = ({ navigation }) => {

    const [loading, setLoading] = useState(true)

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf'),
            'Myriad-Pro-Bold-Condensed': require('../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('../../assets/fonts/Myriad-Pro-Condensed.ttf')

        });
        setLoading(false);}
        fetchData();
    })

    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/pasta1.jpg')}
            style={{height:'100%'}}
            ><View style={{width:'100%', height:'100%'}}>
                <View style={{width:'100%', height:'33%', backgroundColor:'#efceb8'}}>
                <Image 
                resizeMode='contain'
                style={{width:'90%', height:'100%', alignSelf:'center'}}
                source={require('../img/carbonara1.jpg')}
                />
                </View>
                <View style={{width:'100%', height:'67%', borderColor:'white', backgroundColor:'black'}}>
                    <Text style={{fontSize:36, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>
                    Carbonara
                    </Text>
                    <Text style={{fontSize:20 , color:'#efceb8', paddingTop:10, fontFamily:'Myriad-Pro-Condensed', alignSelf:'center', paddingHorizontal:15}}>
                    Carbonara is an Italian pasta dish from Rome, made with egg, hard cheese, cured pork, and black pepper. The dish arrived at its modern form, with its current name, in the middle of the 20th century
                    </Text>
                    <Text style={{color:'#efceb8', fontFamily:'Myriad-Pro-Condensed', fontSize:20, paddingLeft:15}}>
                        -Parmesan cheese {'\n'}
                        -egg yolks{'\n'}
                        -higher-welfare pancetta{'\n'}
                        -garlic{'\n'}
                        -olive oil{'\n'}
                    </Text>
                    <View style={{width:'90%', alignSelf:'center', marginTop:15, flexDirection:'row', justifyContent:'space-between', position:'absolute', bottom:60}}>
                        <Text style={{fontSize:36, fontFamily:'Myriad-Pro-Bold-Condensed', color:'black', paddingHorizontal:20, backgroundColor:'#efceb8'}}>$14.99</Text>
                    <View style={{flexDirection:'row', width:'35%', justifyContent:'space-evenly', alignSelf:'flex-end', marginTop:10}}>
                    <TouchableOpacity
                        style={{padding:5,borderWidth:1, borderColor:'#efceb8', borderRadius:4, }}
                    >
                        <FontAwesome 
                            name='facebook-f'
                            size={25}
                            color='#efceb8'
                            style={{paddingHorizontal:5}}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{padding:5,borderWidth:1, borderColor:'#efceb8', borderRadius:4, alignSelf:'center'}}
                    >
                        <FontAwesome 
                            name='instagram'
                            size={25}
                            color='#efceb8'
                            style={{paddingHorizontal:2}}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{padding:5,borderWidth:1, borderColor:'#efceb8', borderRadius:4, alignSelf:'center'}}
                    >
                        <FontAwesome 
                            name='whatsapp'
                            size={25}
                            color='#efceb8'
                            style={{paddingHorizontal:2}}
                        />
                    </TouchableOpacity>
                    </View>
                    </View>
                </View>
                </View>
            </ImageBackground>
            :null}
        </View>
    )
}

const styles = StyleSheet.create({

});

export default Meal;