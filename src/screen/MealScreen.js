import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity, Image} from 'react-native';
import * as Font from 'expo-font';

const MealScreen = ({ navigation }) => {

    const [loading, setLoading] = useState(true)

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf'),
            'Myriad-Pro-Bold-Condensed': require('../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('../../assets/fonts/Myriad-Pro-Condensed.ttf')

        });
        setLoading(false);}
        fetchData();
    })

    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/pasta1.jpg')}
            style={{height:'100%'}}
            ><View style={{width:'100%', height:'100%'}}>
                <View style={{width:'90%', height:'30%', borderColor:'#efceb8', marginTop:30,flexDirection:'row', padding:10, backgroundColor:'rgba(0,0,0,0.6)', alignSelf:'center'}}>
                <View style={{width:'40%', height:'80%', backgroundColor:'#efceb8',justifyContent:'center', alignSelf:'center'}}>
                        <Image style={{width:'95%', height:'95%', alignSelf:'center'}} source={require ('../img/carbonara.jpg')}/>
                    </View>
                    <View style={{width:'60%', height:'100%', paddingLeft:4}}>
                        <Text style={{fontSize:34, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>Carbonara</Text>
                        <Text style={{fontSize:15, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>Carbonara is an Italian pasta dish from Rome made with egg, hard cheese, cured pork, and black pepper</Text>
                        <TouchableOpacity 
                        onPress={() => navigation.navigate('Meal')}
                        style={{width:'50%', height:'20%', backgroundColor:'#efceb8', alignSelf:'center', marginTop:3, justifyContent:'center'}}>
                            <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MEAL INFO</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{width:'90%', height:'30%', borderColor:'#efceb8', marginTop:30,flexDirection:'row', padding:10, backgroundColor:'rgba(0,0,0,0.6)', alignSelf:'center'}}>
                    <View style={{width:'60%', height:'100%', paddingLeft:4}}>
                    <Text style={{fontSize:34, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center', opacity:1}}>Bolognese</Text>
                        <Text style={{fontSize:15, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>Carbonara is an Italian pasta dish from Rome made with egg, hard cheese, cured pork, and black pepper</Text>
                        <TouchableOpacity style={{width:'50%', height:'20%', backgroundColor:'#efceb8', alignSelf:'center', marginTop:3, justifyContent:'center'}}>
                            <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MEAL INFO</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{width:'40%', height:'80%', backgroundColor:'#efceb8',justifyContent:'center', alignSelf:'center'}}>
                        <Image style={{width:'95%', height:'95%', alignSelf:'center'}} source={require ('../img/bolognese.jpg')}/>
                    </View>
                </View>
                <View style={{width:'90%', height:'30%', borderColor:'#efceb8', marginTop:30,flexDirection:'row', padding:10, backgroundColor:'rgba(0,0,0,0.6)', alignSelf:'center'}}>
                <View style={{width:'40%', height:'80%', backgroundColor:'#efceb8',justifyContent:'center', alignSelf:'center'}}>
                        <Image style={{width:'95%', height:'95%', alignSelf:'center'}} source={require ('../img/pesto.jpg')}/>
                    </View>
                    <View style={{width:'60%', height:'100%', paddingLeft:4}}>
                        <Text style={{fontSize:34, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>Pesto</Text>
                        <Text style={{fontSize:15, color:'#efceb8', padding:4, fontFamily:'Parisienne-Regular', alignSelf:'center'}}>Carbonara is an Italian pasta dish from Rome made with egg, hard cheese, cured pork, and black pepper</Text>
                        <TouchableOpacity style={{width:'50%', height:'20%', backgroundColor:'#efceb8', alignSelf:'center', marginTop:3, justifyContent:'center'}}>
                            <Text style={{fontSize:22, fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center'}}>MEAL INFO</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                </View>
            </ImageBackground>
            :null}
        </View>
    )
}

const styles = StyleSheet.create({

});

export default MealScreen;