import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import * as Font from 'expo-font';
import Entypo from '@expo/vector-icons/Entypo';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';
import {SoupIcon} from '../Icons';

const MenuScreen = ({ navigation }) => {

    const [loading, setLoading] = useState(true)

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf')
        });
        setLoading(false);}
        fetchData();
    })

    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/menu1.jpg')}
            style={{height:'100%'}}
            ><View style={{width:'100%', height:'100%', padding:'5%', flexDirection:'column'}}> 
            <View style={{flexDirection:'row', height:'22%', width:'100%', justifyContent:'space-between', marginTop:30}}>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Meals')}
                style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <Entypo 
                    name='drink'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Drinks</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <MaterialCommunityIcons 
                    name='pizza'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Pizza</Text>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', height:'22%', width:'100%', justifyContent:'space-between', marginTop:30}}>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <MaterialCommunityIcons 
                        name='bowl'
                        color='#efceb8'
                        size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Soup</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <MaterialCommunityIcons 
                    name='fish'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Fish</Text>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', height:'22%', width:'100%', justifyContent:'space-between', marginTop:30}}>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <MaterialCommunityIcons 
                    name='hamburger'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Burgers</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <Entypo
                    name='bowl'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Pasta</Text>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', height:'22%', width:'100%', justifyContent:'space-between', marginTop:30}}>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <Entypo 
                    name='drink'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Drinks</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:'48%', height:'100%', opacity:0.8, backgroundColor:'black', padding:'5%', justifyContent:'center', alignItems:'center', paddingTop:30}}>
                    <MaterialCommunityIcons 
                    name='fish'
                    color='#efceb8'
                    size={40}
                    />
                    <Text style={{color:'white', fontSize:28, fontFamily:'Parisienne-Regular', alignSelf:'center', color:'#efceb8'}}>Fish</Text>
                </TouchableOpacity>
            </View>
            </View>
            </ImageBackground>
            :null}
        </View>
    )
}

const styles = StyleSheet.create({

});

export default MenuScreen;