import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import * as Font from 'expo-font';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'; 
import {MaterialIcons, MaterialCommunityIcons, FontAwesome} from '@expo/vector-icons'


const ReserveScreen = ({ navigation }) => {

    const [loading, setLoading] = useState(true)
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [selectedDate, setSelectedDate] = useState(new Date()); 

    var currentDate = moment().format("DD/MM/YYYY");
    var currentTime = moment().format('LT');

    useEffect( () => {
        const fetchData = async () => {
            const response =  await Font.loadAsync({
            'Parisienne-Regular' : require('../../assets/fonts/Parisienne-Regular.ttf'),
            'Myriad-Pro-Bold-Condensed': require('../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('../../assets/fonts/Myriad-Pro-Condensed.ttf')

        });
        setLoading(false);}
        fetchData();
    })
    return(
        <View>
            {!loading
            ?<ImageBackground 
            source={require('../img/reservation.jpg')}
            style={{height:'100%'}}
            >
                <View style={{width:'100%', height:'100%', backgroundColor:'rgba(0,0,0,0.6)'}}>
                    <View style={{width:'80%', alignSelf:'center', alignItems:'center', marginTop:80}}>
                        <Text style={{fontSize:50, fontFamily:'Parisienne-Regular', color:'#efceb8'}}>Make a</Text>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', color:'white'}}>R E S E R V A T I O N</Text>
                        <Text style={{color:'white',textAlign:'center', marginTop:50, fontSize:16}}>
                            Booking a table online is easy and takes {'\n'}
                            just a couple of minutes
                            </Text>
                        <View style={{width:'90%'}}>
                            <View style={{width:'100%', flexDirection:'row', justifyContent:'space-between', marginTop:80}}>
                                <TouchableOpacity style={{width:'45%', borderColor:'#efceb8', borderWidth:1, padding:5,paddingHorizontal:10, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{fontSize:18, color:'#efceb8', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>{currentDate}</Text>
                                    <MaterialIcons 
                                    name='date-range'
                                    color='#efceb8'
                                    size={30}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:'45%', borderColor:'#efceb8', borderWidth:1, padding:5,paddingHorizontal:10, flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{fontSize:18, color:'#efceb8', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>{currentTime}</Text>
                                    <MaterialIcons 
                                    name='access-time'
                                    color='#efceb8'
                                    size={30}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{width:'100%', borderWidth:1, borderColor:'#efceb8', justifyContent:'space-between', flexDirection:'row', alignItems:'center', marginTop:20, paddingHorizontal:10, padding:2}}>
                                <Text style={{fontSize:22, color:'#efceb8', fontFamily:'Myriad-Pro-Condensed', paddingRight:8, padding:4}}>Marko Petrovic</Text>
                            </View>
                            <View style={{width:'100%', borderWidth:1, borderColor:'#efceb8', justifyContent:'space-between', flexDirection:'row', alignItems:'center', marginTop:20, paddingHorizontal:10, padding:2}}>
                                <Text style={{fontSize:22, color:'#efceb8', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>4 People</Text>
                                <MaterialCommunityIcons
                                name='account-group'
                                color='#efceb8'
                                size={35}                                
                                />
                            </View>

                            <TouchableOpacity
                            style={{width:'100%', backgroundColor:'#efceb8', marginTop:20, alignItems:'center'}}
                            >
                                <FontAwesome 
                                name='check'
                                size={40}
                                color='black'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {showDatePicker 
                        ?<DateTimePicker
                        mode="date"
                        display="spinner"
                        defaultDate={new Date()}
                        value={new Date()}
                        onChange={(event, value) => {
                          setShowDatePicker(!showDatePicker);
                          setSelectedDate(value);
                        }}
                                    />
                        :null
                    }
                </View>
            </ImageBackground>
            :null}
        </View>
    )
}

ReserveScreen.navigationOptions = ({ navigation }) => {
    return{
        title: 'Compass Reservation ',
        headerTitleStyle:{
            fontFamily:'Parisienne-Regular',
            fontSize:28
        },
        headerStyle: {
          backgroundColor: '#efceb8',
        },

    }
}

const styles = StyleSheet.create({

});

export default ReserveScreen;