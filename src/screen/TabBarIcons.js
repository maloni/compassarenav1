import React from 'react';
import { Text, View, Image } from 'react-native';

export const TabBarIconsHome = () => {
    return(
        <View style={{flex:1, alignItems:'center',justifyContent:'center', paddingBottom:10}}>
            <Image style={{width:80, height:30}} source={require('../img/home.png')}/>

        </View>
    )
};

export const TabBarIconsRoad = () => {
    return(
        <View style={{flex:1, alignItems:'center',justifyContent:'center', paddingBottom:12}}>
            <Image style={{width:45, height:45}} source={require('../img/tripadvisor.png')}/>

        </View>
    )
};

export const TabBarIconsAccount = () => {
    return(
        <View style={{flex:1, alignItems:'center',justifyContent:'center', paddingBottom:10}}>
            <Image style={{width:50, height:50}} source={require('../img/useracc.png')}/>

        </View>
    )
};

export const TabBarIconsMore = () => {
    return(
        <View style={{flex:1, alignItems:'center',justifyContent:'center', paddingBottom:10}}>
            <Image style={{width:65, height:65}} source={require('../img/more.png')}/>

        </View>
    )
};

